import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NotesComponent } from './notes.component';
import { TitleComponent } from './title/title.component';
import { ContentComponent } from './content/content.component';
import { NewComponent } from './new/new.component';


@NgModule({
  declarations: [
    NotesComponent, 
    TitleComponent, 
    ContentComponent, 
    NewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
    NotesComponent, 
    TitleComponent, 
    ContentComponent,
    NewComponent
  ],
})
export class NotesModule { }
