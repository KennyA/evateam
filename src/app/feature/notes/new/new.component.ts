import { Component } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';

import { Note } from '../notes.component'

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent {

  @Output() newNoteEvent = new EventEmitter<Note>();

  newNoteForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl('')
  });

  constructor() {}

  ngOnInit(): void {
  }

  onSubmit(): void {
    const form = this.newNoteForm.value;
    if (form.title && form.content) {
      this.newNoteEvent.emit(form as Note);
      this.newNoteForm.reset();
    }
  }

}
