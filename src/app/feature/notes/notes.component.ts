import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

export class Note {
  title: string;
  content: string;
}

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})

export class NotesComponent {

  noteId: number = -1;
  noteNew: boolean = false;
  notes: Note[] = [
    {title: 'test title 1', content: 'test content 1'},
    {title: 'test title 2', content: 'test content 2'},
  ];

  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.noteId = -1;
        this.noteNew = false;
        if (/^(\/\d)$/.test(event.url)) {
          const routeId = Number(event.url.substring(1));
          if (routeId <= this.notes.length - 1) {
            this.noteId = routeId;
          }
        } else if (event.url === '/new') {
          this.noteNew = true;
        }
      }
    })
  }

  ngOnInit(): void {
  }

  addNote(newNote: Note) {
    this.notes.push(newNote);
  }

}
