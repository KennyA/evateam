import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent {

  @Input() active: boolean;
  @Input() title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
