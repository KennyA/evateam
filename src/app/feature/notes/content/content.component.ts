import { Component, Input } from '@angular/core';

import { Note } from '../notes.component'

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {

  @Input() note: Note;

  constructor() { }

  ngOnInit(): void {
  }

}
