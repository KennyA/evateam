import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesComponent } from './feature/notes/notes.component';


const routes: Routes = [
  { path: ':id', component: NotesComponent },
  { path: '**', component: NotesComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
